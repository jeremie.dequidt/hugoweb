---
title: "Test: Linux commands"
subtitle: "Practice with folders, file and basic commands"
tags: ["linux", "practical"]
categories: ["Basic of Computer Science (SE3)", "Structured Programming (SE2A3)"]
date: 2017-06-01T10:18:11+02:00
draft: false
---

## Tree Navigation

- Open the terminal and navigate to your home directory.
- Create a directory called `cs-practical` and navigate into it.
- Create three subdirectories inside `cs-practical` named `folder1,` `folder2,` and `folder3.`
- Navigate into `folder1` and create a file called `file1.txt.`
- Navigate back to the parent directory and use the `ls` command to verify the existence of `folder1,` `folder2,` and `folder3.`

##  File Editing

- Navigate into `folder1` and open `file1.txt` using a command-line text editor like `nano` or `vim`.
- Write a short message inside the file, such as `Hello, World!`
- Save the changes and exit the text editor.
- Use the `cat` command to display the contents of `file1.txt` on the terminal.

## File Searching

- Navigate to the `cs-practical` directory.
- Use the `find` command to search for all files within `cs-practical` that end with `.txt`.
- Run the command to search for files and observe the output.
- Repeat the search, but this time, redirect the output to a file called `filelist.txt` using the `>` symbol.

## Basic Commands

- Create a new directory called `temp` within `cs-practical.`
- Use the `cp` command to copy `file1.txt` into the `temp` directory.
- Rename the copied file to `newfile.txt` using the `mv` command.
- Remove the original `file1.txt` from `folder1` using the `rm` command.
- Use the `ls` command to verify that `newfile.txt` is present in the `temp` directory.
- Navigate to the home directory using the `cd` command.

## Bonus Challenge

- Use the `grep` command to search for the word `World` within all files in the `cs-practical` directory.
- Combine the `grep` command with the `wc` command to count the number of occurrences of the word `World` in the files.

## Redirections

- Create a new file called `input.txt` in the `cs-practical` directory and write a few lines of text in it.
- Use the cat command along with input redirection (`<`) to display the contents of `input.txt` on the terminal.
- Create a new file called `output.txt` and use output redirection (`>`) to redirect the output of the ls command into `output.txt`.
- Verify that `output.txt` contains the list of files in the current directory.

## Pipes

- Use the `ls` command to list all the files in the current directory and pipe (`|`) the output to the grep command to search for files with the extension `.txt`.
- Modify the previous command to also pipe the output to the wc command with the `-l` flag to count the number of files with the `.txt` extension.

## Simple `sed` Usage

- Create a new file called `data.txt` in the `cs-practical` directory and write a few lines of text in it, including some occurrences of the word `Hello.`
- Use the `sed` command to replace all occurrences of `Hello` with `Hi` in `data.txt` and display the modified contents on the terminal.
- Redirect the modified contents to a new file called `modified.txt` using output redirection (`>`).

## Advanced `sed` Usage (Bonus Challenge)

- Use the `sed` command to remove all lines containing the word `Lorem` from a file called `lorem.txt` and display the modified contents on the terminal.
- Redirect the modified contents to a new file called `filtered_lorem.txt` using output redirection (`>`).