---
title: "First Steps with Linux"
subtitle: "Practice with folders, file and basic commands"
tags: ["linux", "practical"]
categories: ["Basic of Computer Science (SE3)", "Structured Programming (SE2A3)"]
date: 2017-05-31T10:18:11+02:00
draft: false
---

{{< admonition type=abstract title="Objectives" open=true >}}
This practical work includes :
- [ ] file system of linux
- [ ] file manipulation (navigation, editing, search)
- [ ] basic commands
- [ ] some tips
{{< /admonition >}}

## Linux File System

### Different categories of files

The different categories of files are:

__Normal__ files
: text: mail, program sources, scripts, configuration...
: executables: programs in binary code

__Directory__ files (we talk about directory)
: they are container files that contain references to other files true framework
of the tree structure, they allow to organize files by categories.

__Special__ files
: located in ```/dev```, these are access
points prepared by the system to the devices. Mounting will perform a mapping of
these special files to their directories.

__Link__ files
: These are files that contain only a reference (pointer) to another file. This
makes it possible to use the same file under several names without having to
duplicate it on the disk.

### Application

The command `ls` allows you to see a part of the tree structure. For example, on
my personal account, the command `ls -l` (`-l` gives details about the contents
of the tree) gives the following result.

{{< highlight txt >}}
drwx------ 2 jdequidt imaEns  4096 déc.   8  2011 Desktop
drwxr-x--- 2 jdequidt imaEns  4096 sept. 28  2010 Documents
drwxr-x--- 2 jdequidt imaEns  4096 déc.  18  2018 Downloads
lrwxrwxrwx 1 jdequidt imaEns    29 sept.  9 14:09 mixed.csv -> ima2a/mixed_all_unordered.csv
drwxr-x--- 2 jdequidt imaEns  4096 sept. 28  2010 Music
drwxr-x--- 2 jdequidt imaEns  4096 sept. 28  2010 Pictures
-rwxr-xr-x 1 jdequidt imaEns 33024 sept.  9 14:05 prog1
drwxr-x--x 2 jdequidt imaEns  4096 sept.  9 14:08 Public
drwxr-x--x 2 jdequidt imaEns  4096 juin   6 09:56 public_html
-rw-r--r-- 1 jdequidt imaEns   949 sept.  9 14:05 ReadMe.md
drwxr-xr-x 6 jdequidt imaEns  4096 août  27 09:51 tmp
drwxr-x--- 2 jdequidt imaEns  4096 sept. 28  2010 Videos
{{< /highlight >}}

From the following link
[https://www.garron.me/en/go2linux/ls-file-permissions.html](https://www.garron.me/en/go2linux/ls-file-permissions.html),
identify the:

- directories
- the _text_ files
- the executable file
- the symbolic link and to which real file it points
- the size in bytes of the file `ReadMe.md`.
- the directory that contains the most subdirectories

## Get Started

### References

To have a very synthetic view of the main commands, I advise you to consult the
following links:
[https://dev.to/awwsmm/101-bash-commands-and-tips-for-beginners-to-experts-30je](https://dev.to/awwsmm/101-bash-commands-and-tips-for-beginners-to-experts-30je)
(very well structured) or
[https://bookmarks.ecyseo.net/?EAWvDw](https://bookmarks.ecyseo.net/?EAWvDw)

### Preamble

1. Once you are logged in, launch a terminal (`Terminal`, `Konsole`...)
1. Launch a program (for example `xeyes`) in interactive mode `xeyes &` and in
   non-interactive mode `xeyes`. What is the difference? Type the command
   `CTRL+C` to kill the `xeyes` process.

## Files and Folders

### Directory navigation

1. When you launch the terminal, in which directory are you located?
1. Type (successively) the following commands:

{{< highlight bash "lineNos=inline" >}}
cd
pwd
cd /tmp
pwd
cd ~
pwd
cd /tmp
cd /usr/local
pwd
cd -
pwd
{{< /highlight >}}

1. Look at the result. Which directory does the `~` (tilde) match? You can
   always go to your main / home directory, your _home_ (in system jargon) by
   simply doing `cd` or `cd ~`. Similarly `cd -` allows you to go to the
   directory where you were previously.
1. Create the `se3` directory in your main directory :

{{< highlight bash >}}
mkdir ~/se3
cd ~/se3
{{< /highlight >}}

or equivalent with :

{{< highlight bash >}}
cd
mkdir se3
cd se3
{{< /highlight >}}

1. Display the contents of this directory with the command `ls`.
1. Create an empty file (in the `se3` directory) named `empty_file.txt` with the
   `touch empty_file.txt` command.
1. Display the contents of your `se3` directory
1. It can also be used in this way:

{{< highlight bash >}}
cd /tmp
ls ~/se3
{{< /highlight >}}

1. Type `cd` to return to your _home_.
1. What is the content of your _home_?
1. Create a `tmp` directory. What is now the content of your _home_?
1. Delete the `tmp` directory with the `rm` command.
1. Display the contents of your _home_.
1. Try deleting `tmp` again, what happens?
1. Be the following command sequence:

{{< highlight bash >}}
cd /tmp
ls
ls -a
ls -l
ls -l -a
ls -la (equivalent to the previous one)
{{< /highlight >}}

1. What is the difference in display? What are the "the" and "a" options used
   for?
1. Delete the `se3` directory with its contents via:

{{< highlight bash >}}
cd ~
rm -rf se3
{{< /highlight >}}

See the help of `rm` (via `man rm`) and tell why this command is dangerous.

### Search in text

1. By always being in the `/etc/dictionaries-common` directory
1. What does the command `wc <filename>` do? What do the `-` and `-c` options do?
1. What does the `grep` command do with e.g. the `grep house words` or `grep
   house words` commands?

### Edit / modify a file

1. In your home directory, create a text file using the `vim` editor via the
   command `vim test.txt`.
1. Press the `i` key to switch to `vim` insertion mode, you can then type text
   (e.g. your first and last name).
1. Then press `ESC` to exit insertion mode.
1. Then press `:w` to save the file and then `:q` to exit `vim` (it is possible
   to combine the 2 commands into one by typing `:wq`)
1. Check with `less` that the file contains what you wrote.
1. How big is this file?

### Search for files in a directory

1. Consult the help of the search command with `man find`.
1. Then type:

{{< highlight bash >}}
find /etc
find /etc -name "*.d"
find /etc -name "*.cfg" -ls
ls -R /etc
{{< /highlight >}}

1. What is the meaning of the character `*` here?
1. What is the purpose of the `-name` option?
1. What is the difference between `ls -R` and `find`?
1. Try the two commands `find /etc -exec wc '{}' +` and `find /etc -name "*.cfg"
   -exec wc '{}' +`. What does the `-exec` option do?

## Redirections

### Standard Input (stdin)

We remind you that the standard input corresponds to what is typed on the keyboard.

1. What does the `sort` command do? Look specifically at what happens when
   `sort` has no file as a parameter.
1. Try it:

{{< highlight bash >}}
sort
a
d
c
{{< /highlight >}}

then type `CTRL+D`.

1. Similarly, try to sort the numbers 2, 11, 1. Explain why 11 is considered to
   be smaller than 2. Use the appropriate option to get a numeric sort.

### Standard Output (stdout)

- What does the `echo` command do?
- Do the following (notice that the second call to `echo` overwrites the
    contents of `file`):

{{< highlight bash >}}
echo test
echo test > file
cat file
echo toto
echo toto > file
cat file
{{< /highlight >}}

- With `>>` we add at the end without overwriting the file

{{< highlight bash >}}
echo test > file2
cat file2
echo toto >> file2
cat file2
{{< /highlight >}}

- Using `sort`, generate an .sorted file containing the numbers 2, 1, 11 sorted
    in ascending order. The result is displayed in the terminal?
- Type the following commands and explain what happens:

{{< highlight bash >}}
sort < file.sorted
sort < file.sorted > file.sorted2
cat .sorted2 file
{{< /highlight >}}

- Why are the `file.sorted` and `file2.sorted` files different? You can use the
    `diff` command to see the differences.

### The `pipe`

The `pipe` allows to connect the result of a command to the input of a new command.

1. View the lines in `file.sorted` that contain the __1__ character (look at the
   `grep` command).
1. Count the number of lines in this file
1. How do I count the number of lines that contain __1__ in `file.sorted`?
1. Try `grep 1 file.sorted | wc` to check that this command answers the previous
   question and that it does not generate temporary files.

## Configurations / remote working

### The `.bashrc` file

This file contains commands that are executed each time you open a terminal.
This file usually contains shortcuts or options for the terminal.

1. With a text editor[^1], create / open your `.bashrc` file and write `alias
   ll='ls -l'` in it.
1. Exit the editor and type `ll`. What happens? Open a new terminal (without
   closing the first one) and type `ll`. What happens? In the first terminal,
   type `source .bashrc` and check that the command `ll` now works.
1. Reload `.bashrc` in the terminal
1. You can enrich your `.bashrc` file to customize your terminal with for
   example:

{{< highlight bash >}}
# Set environment variables
export CLICOLOR=1
export PGHOST=serveur-etu.polytech-lille.fr
export GREP_OPTIONS='--color=auto'

# Aliases
alias ll='ls -alF --color=auto' 

# Custom prompt
PS1='\[\e[1;32m\]\u@\h \[\e[1;34m\]\w \[\e[1;31m\]\$ \[\e[0m\]'
{{< /highlight >}}

If you wish to have more information on the possibilities of customization, you can consult the following link [https://www.howtogeek.com/307701/how-to-customize-and-colorize-your-bash-prompt/](https://www.howtogeek.com/307701/how-to-customize-and-colorize-your-bash-prompt/)

### _Very_ useful commands for these 3 years

You will find below some commands that will be very useful for this year (and
used very often in TP control for example):

1. View / copy _public_ files from another account. If you know the login of
   another person of Polytech Lille, you can consult the authorized files of
   this account via:

{{< highlight bash >}}
ls ~your_login
{{< /highlight >}}

1. Connect to the school's machines/servers from the outside:

{{< highlight bash >}}
ssh votre_login@portier.polytech-lille.fr -p 2222
{{< /highlight >}}

at Polytech Lille, you can connect to another machine with the following command

{{< highlight bash >}}
ssh your_login@machine_name
{{< /highlight >}}

such as for example:

{{< highlight bash >}}
ssh your_login@gayant01
{{< /highlight >}}

### Useful shortcuts

The terminal uses a specific software library called
[Readline](https://en.wikipedia.org/wiki/GNU_Readline) to read the commands you
type. It provides several shortcuts to improve your productivity. These
shortcuts could be used either using `emacs` syntax or `vi` syntax. By default,
the `emacs` syntax is enabled. Below are the most useful shortcuts (the complete
list is detailed in the previous link).

__Tab__
: auto-complete commands and files

__Up Arrow___ or __Down Arrow__
: allow to navigate through your command history (previous command or next command)

__CTRL + r__ or __CTRL + s__
: backward or forward search in your command history

__CTRL + a__ / __CTRL + f__
: moves the cursor to the beginning / end of the line

__ALT + b__ / __ALT + f__
: moves the cursor backward / forward one word

__CTRL + l__
: clears the screen

__CTRL + u__
: clears the line and copies it to the clipboard

__CTRL + k__
: clears the line after the cursor and copies it to the clipboard

__CTRL + w__
: remove one word backwards from cursor position

### Back to file permissions

1. Based on the
   [https://www.linuxtricks.fr/wiki/droits-sous-linux-utilisateurs-groupes-permissions](https://www.linuxtricks.fr/wiki/droits-sous-linux-utilisateurs-groupes-permissions)
   page, change the read/write/execute permissions on your files.
1. Create a text file in your `Public` directory and make it accessible and
   readable (but not editable) by all people from the group `ima3`.

## Your first `C` program

1. Write the following `C` program

{{< highlight c >}}
#include <stdio.h>

int main()
{
    printf("Hello World !\n");
    return 0;
}
{{< /highlight >}}

1. Compile your program with the command `clang myfile.c` or `gcc myfile.c`.
1. Run your program with `./a.out`.

[^1]: `nano` or `gedit` for instance... we will use `vim` later
