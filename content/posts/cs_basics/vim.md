---
title: "VIM editor"
subtitle: "Practice with an advanced editor"
date: 2017-05-31T11:00:14+02:00
tags: ["linux", "practical"]
categories: ["Basic of Computer Science (SE3)", "Structured Programming (SE2A3)"]
draft: false
---

{{< admonition type=abstract title="Objectives" open=true >}}
This practical work includes :
- [ ] basic editing (copy / paste / replace / search)
- [ ] file navigation
- [ ] modes
- [ ] configuration
{{< /admonition >}}

## Principle

To launch the editor, just type `vim` in a terminal. Unlike usual text editors,
`vim` has 3 modes of operation: _interactive_ mode, _insertion_ mode and
_command_ mode:

__interactive__ mode
: is the default mode. It is the one you are in when you start `vim` or when
you are in another mode and press the `ESC` key. In this mode, you can do
operations like moving in a file, copying and pasting, deleting text, replacing
text...

__insertion__ mode
: is the mode that allows you to write text. From the interactive mode, you
just have to press the `i` key to switch to the insertion mode.

__command__ mode
: is the mode that allows you to execute `vim` commands or system commands.
From the interactive mode, you have to press the `:` key and then type the
command you want to execute.

## First steps

1. run `vim` in a terminal
1. press `i` to switch to insert mode
1. type a minimal `C` program
1. to save, press `ESC` (to exit insert mode) and then type `:w test.c` to save
   what you have written (`:` is the command to enter command mode, `w` is the
   save -write command and `test.c` is the file name)
1. Still in interactive mode, move the file around with the `h` (left), `j` (
   down), `k` (up), `l` (right) keys. You can also use the arrow keys on the
   keyboard.
1. To quit `vim`, just type `:q` or `:q!` if you want to force the closure (by
   default `vim` does not close if there are unsaved changes).

> It is possible to save and exit with the command `:wq`.

## Navigation

In addition to the `h`, `j`, `k`, `l` keys, others are useful such as:

- `0` which automatically returns to the beginning of the current line
- `$` which allows to go automatically to the end of the current line
- `gg` which allows to go to the first line of the file
- `G` which allows to go to the last line of the file
- `xG` where `x` is a number allows you to go to the line `x`
- `e` which goes to the end of the current word
- `b` which goes to the previous word
- `w` which goes to the next word
- `{` / `}` goes to the beginning / end of the current paragraph
- `%` goes to the matching brace or parenthesis inside code

## Cut / Delete

- To delete a character, type `x` or `yx` where `y` is the number of characters
    to delete
- To delete a line, type `dd` or `xdd` where `x` is the number of lines to
    delete
- To delete a word, type `dw` (_delete word_) or `xdw` where `x` is the number
    of words to delete
- To delete from the beginning of the line to the cursor, type `d0`.
- To delete from the cursor to the end of the line, type `d$`.

## Copy

Same as cut, except the command is `yy` to copy a line, `yw` to copy a word,
`y0` to copy...

## Paste

To paste text, type `p` or `xp` where `x` is the number of times to copy. Be
careful, the copy is done immediately after the cursor, there is no line break.

## Visual Cut / Copy

If you want to cut / copy a specific region of text, you can type `v` in
command mode to enter visual mode and then by using arrows you can highlight
the region you want to edit. After that use cut / copy command to go back to
command mode.

## Replace

- To replace a letter, just place the cursor on the line to be replaced and
    type `rs` where `s` is the new letter to be inserted
- To replace text, it is possible to use the syntax `:s/old/new` which replaces
    the first occurrence of `old` with `new`, variants exist:
  - `:s/old/new/g`: replaces all occurrences of the line where the cursor is
  located
  - `:#,#s/old/new/g`: replaces all occurrences in lines # to # of the file
  - `:%s/old/new/g`: replaces all occurrences in the whole file

## Undo changes

- To undo a change, type `u` or `xu` where `x` is the number of times to go back

## Search for a word

- To search for a word after the cursor position, type `/word`. To go to the
    next occurrence, type `n` or `N` for the previous occurrence
- To search for a word before the cursor position, type `?word`, the rest is
    the same

## Launch an external command

To do this, just type the name of the command preceded by `:!`, for example
`:!ls` to display the contents of the current directory

## Open another file

In `vim` just type `:e filename` to open the file named `filename`. You can
then navigate between files with `:e #x` where `x` is the file number.

## The configuration file

There are lots of options in `vim` to make it more user friendly (syntax
highlighting, line numbering, aliases...). Here are a few that you can write in
a `.vimrc` file that you put in the root of your home directory (so you don't
have to enable the options every time you open it).

- `set nocp` disables compatibility mode
- `syn on` enables syntax highlighting
- `set syntax =on` enables syntax highlighting
- `filetype indent plugin on` indent according to file type
- `set nu` displays the line number
- `set showmatch` show missing braces / parenthesis
- `set tabstop =4` tab size in spaces
- `set shiftwidth =4` size in tab spaces
- `set softtabstop =4` size in tab spaces
- `set expandtab` turns tabs into spaces
- `set cursorline` highlights the line where the cursor is located
- `iab #i #include` example of an alias, now just type `#i` then tab and it
    will be replaced by `#include`

{% highlight viml %}
set nocompatible
filetype off
filetype plugin indent on

set nu
set ai
set si
set mouse=a
set showmatch
set tabstop =4
set shiftwidth =4
set softtabstop =4
set expandtab
set cursorline
set wrap
set textwidth=80
set wrapmargin=2
iab #i #include
iab #d #define
{% endhighlight %}

## Being efficient with vim

- You can enable spelling in `vim` with the command `set spell spelllang=en` (
    for english or `fr` for french). When spelling is enables you can jump to
    the spelling errors with __\[s__ (forward jump)  __]s__ (backward jump).
- You can correctly indent a whole file by typing __gg=G__ (__gg__ goes to the
    beginning of the file, __=__ is the indent command and __G__ goes to the end
    of file)... you can even go back to the line you were with (__gg=G''__).
- You can go to any file referred in you source code by typing __gf__ when the
    cursor is under the file name. When you want to close the visited file you
    can type __bw__
- You can split the window horizontally resp. vertically with `:sp` resp. `:vsp`
    (and this can be done recursively). Once you have a split window, you can
    navigate with __CTRL w + arrow__ to navigate and __CTRL w + c__ to close the
    current split (and even __CTRL w + o__ to close all the split except the
    active one).
- You can easily change the case of text in visual mode: once you have some
    selected text, press __U__ for uppercase, __u__ for lowercase and __~__ to
    switch case.
- For insertion, __a__ insert at the end of the current line, __o__ creates a
    new blank line after the current one, __O__ creates a new blank line before
    the current one.

There are many other shortcuts like bookmarks in file, multiple items in the
clipboard, repeat the latest insertion command... but it is up to you to find
the ones that makes you more efficient and productive in your workflow... bu
remember __practice is key !__

## Plugins

`vim` could be upgraded with many plug-ins that can help for the routine tasks.
A popular plug-in manager is `Vundle`. One installed you can for instance have:

- `Plugin 'dense-analysis/ale'` is a syntax analyzer that highlights on the fly
    errors in warnings while you are typing. This can helps you to correct many
    errors before trying to compile your code. By default warning are
    highlighted with `--` while errors are highlighted with `>>`. The status bar
    gives some details about the error. Adding `nmap <silent> <C-k>
    <Plug>(ale_previous_wrap) nmap <silent> <C-j> <Plug>(ale_next_wrap)` in your
    `.vimrc` allows to jump to previous / next errors and warnings with __CTRL +
    j__ and __CTRL + k__.
- `Plugin 'scrooloose/nerdtree'` allows you to have a sidebar with files in
    your current folder. If you have the commande `nnoremap <Leader>n
    :NERDTreeToggle<CR>` in your `.vimrc`, you can open / close the file sidebar
    with the command __\n__
- `Plugin 'preservim/nerdcommenter'` allows you to (un)comment line(s). For
    instance __\cn__ comment the current line and __\cu__ uncomment the current
    line (you can comment / uncomment multiple lines in visual mode.

To install vundle you should:

{% highlight bash %}
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
{% endhighlight %}

then edit your `.vimrc` file with

{% highlight viml %}
set nocp
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'               "Packet repository
Plugin 'dense-analysis/ale'                 "Syntax analyzer
Plugin 'scrooloose/nerdtree'                "File manager
Plugin 'preservim/nerdcommenter'            "Comments
call vundle#end()

syn on
set syntax =on
filetype indent plugin on
set nu
set showmatch
set tabstop =4
set shiftwidth =4
set softtabstop =4
set expandtab
set spellsuggest =5
set cursorline

let g:ale_lint_on_insert_leave = 0
nnoremap <Leader>n :NERDTreeToggle<CR>
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
{% endhighlight %}

then inside `vim` type:

- `:source %` to reload you configuration file
- then `:PluginInstall`

and you should be set-up. If you want to check other plugins, you can visit [
VimAwesome](https://vimawesome.com/)

## Interactive practice

In the terminal, run `vimtutor` and try to finish the tutorial... have fun :)
